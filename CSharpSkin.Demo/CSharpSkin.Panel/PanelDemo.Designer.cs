﻿namespace CSharpSkin.Demo
{
    partial class PanelDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpPanel1 = new CSharpSkin.Panel.CSharpPanel();
            this.cSharpPanel3 = new CSharpSkin.Panel.CSharpPanel();
            this.SuspendLayout();
            // 
            // cSharpPanel1
            // 
            this.cSharpPanel1.BorderColor = System.Drawing.Color.SeaGreen;
            this.cSharpPanel1.Location = new System.Drawing.Point(77, 39);
            this.cSharpPanel1.Name = "cSharpPanel1";
            this.cSharpPanel1.Size = new System.Drawing.Size(141, 146);
            this.cSharpPanel1.TabIndex = 0;
            // 
            // cSharpPanel3
            // 
            this.cSharpPanel3.BorderColor = System.Drawing.Color.Red;
            this.cSharpPanel3.Location = new System.Drawing.Point(77, 229);
            this.cSharpPanel3.Name = "cSharpPanel3";
            this.cSharpPanel3.Radius = 0;
            this.cSharpPanel3.Size = new System.Drawing.Size(294, 150);
            this.cSharpPanel3.TabIndex = 4;
            // 
            // PanelDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 654);
            this.Controls.Add(this.cSharpPanel3);
            this.Controls.Add(this.cSharpPanel1);
            this.Name = "PanelDemo";
            this.Text = "PanelDemo";
            this.ResumeLayout(false);

        }

        #endregion

        private Panel.CSharpPanel cSharpPanel1;
        private Panel.CSharpPanel cSharpPanel3;
    }
}