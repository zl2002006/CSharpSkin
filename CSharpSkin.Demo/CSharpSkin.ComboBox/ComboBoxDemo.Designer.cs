﻿namespace CSharpSkin.Demo
{
    partial class ComboBoxDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpComboBox2 = new CSharpSkin.ComboBox.CSharpComboBox();
            this.cSharpComboBox1 = new CSharpSkin.ComboBox.CSharpComboBox();
            this.cSharpComboBox3 = new CSharpSkin.ComboBox.CSharpComboBox();
            this.SuspendLayout();
            // 
            // cSharpComboBox2
            // 
            this.cSharpComboBox2.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cSharpComboBox2.BaseColor = System.Drawing.Color.Red;
            this.cSharpComboBox2.BorderColor = System.Drawing.Color.Red;
            this.cSharpComboBox2.DropDownHeight = 120;
            this.cSharpComboBox2.FormattingEnabled = true;
            this.cSharpComboBox2.IntegralHeight = false;
            this.cSharpComboBox2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cSharpComboBox2.Location = new System.Drawing.Point(25, 66);
            this.cSharpComboBox2.Name = "cSharpComboBox2";
            this.cSharpComboBox2.Size = new System.Drawing.Size(121, 20);
            this.cSharpComboBox2.TabIndex = 1;
            // 
            // cSharpComboBox1
            // 
            this.cSharpComboBox1.ArrowColor = System.Drawing.Color.White;
            this.cSharpComboBox1.BaseColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpComboBox1.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpComboBox1.FormattingEnabled = true;
            this.cSharpComboBox1.Location = new System.Drawing.Point(25, 105);
            this.cSharpComboBox1.Name = "cSharpComboBox1";
            this.cSharpComboBox1.Size = new System.Drawing.Size(121, 20);
            this.cSharpComboBox1.TabIndex = 2;
            // 
            // cSharpComboBox3
            // 
            this.cSharpComboBox3.ArrowColor = System.Drawing.Color.White;
            this.cSharpComboBox3.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.cSharpComboBox3.BorderColor = System.Drawing.Color.Magenta;
            this.cSharpComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cSharpComboBox3.FormattingEnabled = true;
            this.cSharpComboBox3.Location = new System.Drawing.Point(25, 149);
            this.cSharpComboBox3.Name = "cSharpComboBox3";
            this.cSharpComboBox3.Size = new System.Drawing.Size(121, 20);
            this.cSharpComboBox3.TabIndex = 3;
            // 
            // ComboBoxDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpComboBox3);
            this.Controls.Add(this.cSharpComboBox1);
            this.Controls.Add(this.cSharpComboBox2);
            this.Name = "ComboBoxDemo";
            this.Text = "ComboBoxDemo";
            this.ResumeLayout(false);

        }

        #endregion
        private ComboBox.CSharpComboBox cSharpComboBox2;
        private ComboBox.CSharpComboBox cSharpComboBox1;
        private ComboBox.CSharpComboBox cSharpComboBox3;
    }
}