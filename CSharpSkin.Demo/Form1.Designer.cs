﻿namespace CSharpSkin.Demo
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpContextMenuStrip1 = new CSharpSkin.ContextMenuStrip.CSharpContextMenuStrip();
            this.sToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sdfsfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sdfsfsfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cSharpContextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cSharpContextMenuStrip1
            // 
            this.cSharpContextMenuStrip1.BackColor = System.Drawing.Color.LightBlue;
            this.cSharpContextMenuStrip1.CurrentColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpContextMenuStrip1.CurrentOpacity = 50;
            this.cSharpContextMenuStrip1.IsOpenAnimation = true;
            this.cSharpContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sToolStripMenuItem,
            this.toolStripSeparator1,
            this.sdfsfToolStripMenuItem,
            this.toolStripSeparator2,
            this.sdfsfsfToolStripMenuItem,
            this.toolStripMenuItem2});
            this.cSharpContextMenuStrip1.Name = "cSharpContextMenuStrip1";
            this.cSharpContextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.cSharpContextMenuStrip1.Size = new System.Drawing.Size(181, 126);
            // 
            // sToolStripMenuItem
            // 
            this.sToolStripMenuItem.Name = "sToolStripMenuItem";
            this.sToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sToolStripMenuItem.Text = "s";
            // 
            // sdfsfToolStripMenuItem
            // 
            this.sdfsfToolStripMenuItem.Name = "sdfsfToolStripMenuItem";
            this.sdfsfToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sdfsfToolStripMenuItem.Text = "sdfsf";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // sdfsfsfToolStripMenuItem
            // 
            this.sdfsfsfToolStripMenuItem.Name = "sdfsfsfToolStripMenuItem";
            this.sdfsfsfToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sdfsfsfToolStripMenuItem.Text = "sdfsfsf";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem2.Text = "123123123";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.BackColor = System.Drawing.Color.PaleTurquoise;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ContextMenuStrip = this.cSharpContextMenuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.cSharpContextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ContextMenuStrip.CSharpContextMenuStrip cSharpContextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem sdfsfToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem sdfsfsfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
    }
}

