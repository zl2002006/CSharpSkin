﻿namespace CSharpSkin.Demo
{
    partial class GroupBoxDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpGroupBox1 = new CSharpSkin.GroupBox.CSharpGroupBox();
            this.cSharpGroupBox2 = new CSharpSkin.GroupBox.CSharpGroupBox();
            this.cSharpGroupBox3 = new CSharpSkin.GroupBox.CSharpGroupBox();
            this.SuspendLayout();
            // 
            // cSharpGroupBox1
            // 
            this.cSharpGroupBox1.GroupBoxColor = System.Drawing.Color.Gray;
            this.cSharpGroupBox1.Location = new System.Drawing.Point(12, 23);
            this.cSharpGroupBox1.Name = "cSharpGroupBox1";
            this.cSharpGroupBox1.Size = new System.Drawing.Size(217, 72);
            this.cSharpGroupBox1.TabIndex = 0;
            this.cSharpGroupBox1.TabStop = false;
            this.cSharpGroupBox1.Text = "cSharpGroupBox1";
            // 
            // cSharpGroupBox2
            // 
            this.cSharpGroupBox2.GroupBoxColor = System.Drawing.Color.Red;
            this.cSharpGroupBox2.Location = new System.Drawing.Point(12, 127);
            this.cSharpGroupBox2.Name = "cSharpGroupBox2";
            this.cSharpGroupBox2.Size = new System.Drawing.Size(316, 120);
            this.cSharpGroupBox2.TabIndex = 1;
            this.cSharpGroupBox2.TabStop = false;
            this.cSharpGroupBox2.Text = "cSharpGroupBox2";
            // 
            // cSharpGroupBox3
            // 
            this.cSharpGroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cSharpGroupBox3.GroupBoxColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpGroupBox3.Location = new System.Drawing.Point(12, 276);
            this.cSharpGroupBox3.Name = "cSharpGroupBox3";
            this.cSharpGroupBox3.Size = new System.Drawing.Size(316, 120);
            this.cSharpGroupBox3.TabIndex = 2;
            this.cSharpGroupBox3.TabStop = false;
            this.cSharpGroupBox3.Text = "cSharpGroupBox3";
            // 
            // GroupBoxDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpGroupBox3);
            this.Controls.Add(this.cSharpGroupBox2);
            this.Controls.Add(this.cSharpGroupBox1);
            this.Name = "GroupBoxDemo";
            this.Text = "GroupBoxDemo";
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox.CSharpGroupBox cSharpGroupBox1;
        private GroupBox.CSharpGroupBox cSharpGroupBox2;
        private GroupBox.CSharpGroupBox cSharpGroupBox3;
    }
}