﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.Demo
{
    public partial class TreeViewDemo : Form
    {
        public TreeViewDemo()
        {
            InitializeComponent();
            
        }

        private void cSharpTreeView2_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            //e.Node
            this.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd");
        }

        private void cSharpTreeView2_MouseClick(object sender, MouseEventArgs e)
        {
            TreeNode treeNode= this.cSharpTreeView2.GetNodeAt(e.Location);
            if (treeNode!=null)
            {
                this.Text = treeNode.Text;
            }
        }

        private void TreeViewDemo_Load(object sender, EventArgs e)
        {
            this.cSharpTreeView3.MoveNodeStateStyle = new TreeView.TreeViewNodeStateStyle() { Pen = new Pen(Color.Red), SolidBrush = new SolidBrush(Color.IndianRed) };
            this.cSharpTreeView3.SelectFocusNodeStateStyle = new TreeView.TreeViewNodeStateStyle() { Pen = new Pen(Color.Red), SolidBrush = new SolidBrush(Color.FromArgb(50,Color.IndianRed)) };
        }
    }
}
