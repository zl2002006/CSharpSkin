﻿namespace CSharpSkin.Demo
{
    partial class NumericUpDownDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpNumericUpDown6 = new CSharpSkin.NumericUpDown.CSharpNumericUpDown();
            this.cSharpNumericUpDown5 = new CSharpSkin.NumericUpDown.CSharpNumericUpDown();
            this.cSharpNumericUpDown1 = new CSharpSkin.NumericUpDown.CSharpNumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpNumericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpNumericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpNumericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // cSharpNumericUpDown6
            // 
            this.cSharpNumericUpDown6.ArrowColor = System.Drawing.Color.White;
            this.cSharpNumericUpDown6.BaseColor = System.Drawing.Color.Red;
            this.cSharpNumericUpDown6.BorderColor = System.Drawing.Color.Red;
            this.cSharpNumericUpDown6.Location = new System.Drawing.Point(85, 108);
            this.cSharpNumericUpDown6.Name = "cSharpNumericUpDown6";
            this.cSharpNumericUpDown6.Size = new System.Drawing.Size(120, 21);
            this.cSharpNumericUpDown6.TabIndex = 5;
            // 
            // cSharpNumericUpDown5
            // 
            this.cSharpNumericUpDown5.ArrowColor = System.Drawing.Color.White;
            this.cSharpNumericUpDown5.BaseColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpNumericUpDown5.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpNumericUpDown5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cSharpNumericUpDown5.Location = new System.Drawing.Point(85, 240);
            this.cSharpNumericUpDown5.Name = "cSharpNumericUpDown5";
            this.cSharpNumericUpDown5.Size = new System.Drawing.Size(120, 21);
            this.cSharpNumericUpDown5.TabIndex = 4;
            // 
            // cSharpNumericUpDown1
            // 
            this.cSharpNumericUpDown1.ArrowColor = System.Drawing.Color.Salmon;
            this.cSharpNumericUpDown1.BackColor = System.Drawing.Color.Yellow;
            this.cSharpNumericUpDown1.BaseColor = System.Drawing.Color.Lime;
            this.cSharpNumericUpDown1.BorderColor = System.Drawing.Color.Red;
            this.cSharpNumericUpDown1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cSharpNumericUpDown1.Location = new System.Drawing.Point(85, 171);
            this.cSharpNumericUpDown1.Name = "cSharpNumericUpDown1";
            this.cSharpNumericUpDown1.Size = new System.Drawing.Size(120, 21);
            this.cSharpNumericUpDown1.TabIndex = 0;
            // 
            // NumericUpDownDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 439);
            this.Controls.Add(this.cSharpNumericUpDown6);
            this.Controls.Add(this.cSharpNumericUpDown5);
            this.Controls.Add(this.cSharpNumericUpDown1);
            this.DoubleBuffered = true;
            this.Name = "NumericUpDownDemo";
            this.Text = "NumericUpDownDemo";
            ((System.ComponentModel.ISupportInitialize)(this.cSharpNumericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpNumericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpNumericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private NumericUpDown.CSharpNumericUpDown cSharpNumericUpDown1;
        private NumericUpDown.CSharpNumericUpDown cSharpNumericUpDown5;
        private NumericUpDown.CSharpNumericUpDown cSharpNumericUpDown6;
    }
}