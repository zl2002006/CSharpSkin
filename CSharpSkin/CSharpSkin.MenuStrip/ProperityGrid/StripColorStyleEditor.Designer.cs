﻿namespace CSharpSkin.MenuStrip.ProperityGrid
{
    partial class StripColorStyleEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.plBackPressedColor = new System.Windows.Forms.Panel();
            this.lbBackPressedColor = new System.Windows.Forms.Label();
            this.plDropDownImageSeparatorColor = new System.Windows.Forms.Panel();
            this.lbDropDownImageSeparatorColor = new System.Windows.Forms.Label();
            this.lbDropDownImageBackColor = new System.Windows.Forms.Label();
            this.plDropDownImageBackColor = new System.Windows.Forms.Panel();
            this.plForeColor = new System.Windows.Forms.Panel();
            this.lbForeColor = new System.Windows.Forms.Label();
            this.plBackHoverColor = new System.Windows.Forms.Panel();
            this.lbBackHoverColor = new System.Windows.Forms.Label();
            this.plBackNormalColor = new System.Windows.Forms.Panel();
            this.lbBackNormalColor = new System.Windows.Forms.Label();
            this.plBorderColor = new System.Windows.Forms.Panel();
            this.lbBorderColor = new System.Windows.Forms.Label();
            this.plBaseColor = new System.Windows.Forms.Panel();
            this.lbBaseColor = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(168, 309);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(66, 33);
            this.btnOK.TabIndex = 57;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // plBackPressedColor
            // 
            this.plBackPressedColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plBackPressedColor.Location = new System.Drawing.Point(168, 157);
            this.plBackPressedColor.Name = "plBackPressedColor";
            this.plBackPressedColor.Size = new System.Drawing.Size(66, 29);
            this.plBackPressedColor.TabIndex = 56;
            // 
            // lbBackPressedColor
            // 
            this.lbBackPressedColor.AutoSize = true;
            this.lbBackPressedColor.Location = new System.Drawing.Point(11, 161);
            this.lbBackPressedColor.Name = "lbBackPressedColor";
            this.lbBackPressedColor.Size = new System.Drawing.Size(101, 12);
            this.lbBackPressedColor.TabIndex = 55;
            this.lbBackPressedColor.Text = "鼠标按下背景颜色";
            // 
            // plDropDownImageSeparatorColor
            // 
            this.plDropDownImageSeparatorColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plDropDownImageSeparatorColor.Location = new System.Drawing.Point(168, 262);
            this.plDropDownImageSeparatorColor.Name = "plDropDownImageSeparatorColor";
            this.plDropDownImageSeparatorColor.Size = new System.Drawing.Size(66, 29);
            this.plDropDownImageSeparatorColor.TabIndex = 54;
            // 
            // lbDropDownImageSeparatorColor
            // 
            this.lbDropDownImageSeparatorColor.AutoSize = true;
            this.lbDropDownImageSeparatorColor.Location = new System.Drawing.Point(12, 266);
            this.lbDropDownImageSeparatorColor.Name = "lbDropDownImageSeparatorColor";
            this.lbDropDownImageSeparatorColor.Size = new System.Drawing.Size(113, 12);
            this.lbDropDownImageSeparatorColor.TabIndex = 53;
            this.lbDropDownImageSeparatorColor.Text = "下拉图片分割线颜色";
            // 
            // lbDropDownImageBackColor
            // 
            this.lbDropDownImageBackColor.AutoSize = true;
            this.lbDropDownImageBackColor.Location = new System.Drawing.Point(11, 231);
            this.lbDropDownImageBackColor.Name = "lbDropDownImageBackColor";
            this.lbDropDownImageBackColor.Size = new System.Drawing.Size(101, 12);
            this.lbDropDownImageBackColor.TabIndex = 52;
            this.lbDropDownImageBackColor.Text = "下拉图片背景颜色";
            // 
            // plDropDownImageBackColor
            // 
            this.plDropDownImageBackColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plDropDownImageBackColor.Location = new System.Drawing.Point(168, 227);
            this.plDropDownImageBackColor.Name = "plDropDownImageBackColor";
            this.plDropDownImageBackColor.Size = new System.Drawing.Size(66, 29);
            this.plDropDownImageBackColor.TabIndex = 51;
            // 
            // plForeColor
            // 
            this.plForeColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plForeColor.Location = new System.Drawing.Point(168, 192);
            this.plForeColor.Name = "plForeColor";
            this.plForeColor.Size = new System.Drawing.Size(66, 29);
            this.plForeColor.TabIndex = 50;
            // 
            // lbForeColor
            // 
            this.lbForeColor.AutoSize = true;
            this.lbForeColor.Location = new System.Drawing.Point(12, 196);
            this.lbForeColor.Name = "lbForeColor";
            this.lbForeColor.Size = new System.Drawing.Size(53, 12);
            this.lbForeColor.TabIndex = 49;
            this.lbForeColor.Text = "字体颜色";
            // 
            // plBackHoverColor
            // 
            this.plBackHoverColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plBackHoverColor.Location = new System.Drawing.Point(168, 122);
            this.plBackHoverColor.Name = "plBackHoverColor";
            this.plBackHoverColor.Size = new System.Drawing.Size(66, 29);
            this.plBackHoverColor.TabIndex = 48;
            // 
            // lbBackHoverColor
            // 
            this.lbBackHoverColor.AutoSize = true;
            this.lbBackHoverColor.Location = new System.Drawing.Point(12, 126);
            this.lbBackHoverColor.Name = "lbBackHoverColor";
            this.lbBackHoverColor.Size = new System.Drawing.Size(101, 12);
            this.lbBackHoverColor.TabIndex = 47;
            this.lbBackHoverColor.Text = "鼠标进入背景颜色";
            // 
            // plBackNormalColor
            // 
            this.plBackNormalColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plBackNormalColor.Location = new System.Drawing.Point(168, 87);
            this.plBackNormalColor.Name = "plBackNormalColor";
            this.plBackNormalColor.Size = new System.Drawing.Size(66, 29);
            this.plBackNormalColor.TabIndex = 46;
            // 
            // lbBackNormalColor
            // 
            this.lbBackNormalColor.AutoSize = true;
            this.lbBackNormalColor.Location = new System.Drawing.Point(12, 91);
            this.lbBackNormalColor.Name = "lbBackNormalColor";
            this.lbBackNormalColor.Size = new System.Drawing.Size(53, 12);
            this.lbBackNormalColor.TabIndex = 45;
            this.lbBackNormalColor.Text = "背景颜色";
            // 
            // plBorderColor
            // 
            this.plBorderColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plBorderColor.Location = new System.Drawing.Point(168, 52);
            this.plBorderColor.Name = "plBorderColor";
            this.plBorderColor.Size = new System.Drawing.Size(66, 29);
            this.plBorderColor.TabIndex = 44;
            // 
            // lbBorderColor
            // 
            this.lbBorderColor.AutoSize = true;
            this.lbBorderColor.Location = new System.Drawing.Point(12, 56);
            this.lbBorderColor.Name = "lbBorderColor";
            this.lbBorderColor.Size = new System.Drawing.Size(53, 12);
            this.lbBorderColor.TabIndex = 43;
            this.lbBorderColor.Text = "边框颜色";
            // 
            // plBaseColor
            // 
            this.plBaseColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plBaseColor.Location = new System.Drawing.Point(168, 17);
            this.plBaseColor.Name = "plBaseColor";
            this.plBaseColor.Size = new System.Drawing.Size(66, 29);
            this.plBaseColor.TabIndex = 42;
            // 
            // lbBaseColor
            // 
            this.lbBaseColor.AutoSize = true;
            this.lbBaseColor.Location = new System.Drawing.Point(12, 21);
            this.lbBaseColor.Name = "lbBaseColor";
            this.lbBaseColor.Size = new System.Drawing.Size(53, 12);
            this.lbBaseColor.TabIndex = 41;
            this.lbBaseColor.Text = "基础颜色";
            // 
            // StripColorStyleEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 363);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.plBackPressedColor);
            this.Controls.Add(this.lbBackPressedColor);
            this.Controls.Add(this.plDropDownImageSeparatorColor);
            this.Controls.Add(this.lbDropDownImageSeparatorColor);
            this.Controls.Add(this.lbDropDownImageBackColor);
            this.Controls.Add(this.plDropDownImageBackColor);
            this.Controls.Add(this.plForeColor);
            this.Controls.Add(this.lbForeColor);
            this.Controls.Add(this.plBackHoverColor);
            this.Controls.Add(this.lbBackHoverColor);
            this.Controls.Add(this.plBackNormalColor);
            this.Controls.Add(this.lbBackNormalColor);
            this.Controls.Add(this.plBorderColor);
            this.Controls.Add(this.lbBorderColor);
            this.Controls.Add(this.plBaseColor);
            this.Controls.Add(this.lbBaseColor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StripColorStyleEditor";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StripColorStyleEditor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel plBackPressedColor;
        private System.Windows.Forms.Label lbBackPressedColor;
        private System.Windows.Forms.Panel plDropDownImageSeparatorColor;
        private System.Windows.Forms.Label lbDropDownImageSeparatorColor;
        private System.Windows.Forms.Label lbDropDownImageBackColor;
        private System.Windows.Forms.Panel plDropDownImageBackColor;
        private System.Windows.Forms.Panel plForeColor;
        private System.Windows.Forms.Label lbForeColor;
        private System.Windows.Forms.Panel plBackHoverColor;
        private System.Windows.Forms.Label lbBackHoverColor;
        private System.Windows.Forms.Panel plBackNormalColor;
        private System.Windows.Forms.Label lbBackNormalColor;
        private System.Windows.Forms.Panel plBorderColor;
        private System.Windows.Forms.Label lbBorderColor;
        private System.Windows.Forms.Panel plBaseColor;
        private System.Windows.Forms.Label lbBaseColor;
    }
}