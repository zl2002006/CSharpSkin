﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.MenuStrip.ProperityGrid
{
    #region 状态栏，工具栏，菜单栏颜色样式编辑器
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 状态栏，工具栏，菜单栏颜色样式编辑器
    /// </summary>
    public partial class StripColorStyleEditor : Form
    {
        StripColorStyle _stripColorStyle;

        #region 返回状态栏，工具栏，菜单栏颜色样式
        /// <summary>
        /// 返回状态栏，工具栏，菜单栏颜色样式
        /// </summary>
        public StripColorStyle StripColorStyle {
            get {
                return _stripColorStyle;
            }
        }
        #endregion

        #region 无参构造
        /// <summary>
        /// 构造
        /// </summary>
        public StripColorStyleEditor()
        {
            InitializeComponent();
        }
        #endregion

        #region 有参构造
        /// <summary>
        /// 有参构造
        /// </summary>
        /// <param name="stripColorStyle"></param>
        public StripColorStyleEditor(StripColorStyle stripColorStyle)
        {
            _stripColorStyle = stripColorStyle;
            InitializeComponent();
            this.Init();
            this.InitEvent();
        }
        #endregion

        #region 初始化值
        /// <summary>
        /// 初始化值
        /// </summary>
        private void Init()
        {
            this.plBackHoverColor.BackColor = StripColorStyle.BackHoverColor;
            this.plBackNormalColor.BackColor = StripColorStyle.BackNormalColor;
            this.plBackPressedColor.BackColor = StripColorStyle.BackPressedColor;
            this.plBaseColor.BackColor = StripColorStyle.BaseColor;
            this.plBorderColor.BackColor = StripColorStyle.BorderColor;
            this.plDropDownImageBackColor.BackColor = StripColorStyle.DropDownImageBackColor;
            this.plDropDownImageSeparatorColor.BackColor = StripColorStyle.DropDownImageSeparatorColor;
            this.plForeColor.BackColor = StripColorStyle.ForeColor;
        }
        #endregion

        #region 加载事件
        /// <summary>
        /// 加载事件
        /// </summary>
        public void InitEvent()
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel)
                {
                    ct.Click += ColorPannel_Click;
                }
            }
        }
        #endregion

        #region 选择颜色事件
        /// <summary>
        /// 选择颜色事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ColorPannel_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                ((Panel)sender).BackColor = colorDialog.Color;
            }
        }
        #endregion

        #region 确定
        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.StripColorStyle.BackHoverColor = this.plBackHoverColor.BackColor;
            this.StripColorStyle.BackNormalColor = this.plBackNormalColor.BackColor;
            this.StripColorStyle.BackPressedColor = this.plBackPressedColor.BackColor;
            this.StripColorStyle.BaseColor = this.plBaseColor.BackColor;
            this.StripColorStyle.BorderColor = this.plBorderColor.BackColor;
            this.StripColorStyle.DropDownImageBackColor = this.plDropDownImageBackColor.BackColor;
            this.StripColorStyle.DropDownImageSeparatorColor = this.plDropDownImageSeparatorColor.BackColor;
            this.StripColorStyle.ForeColor = this.plForeColor.BackColor;
            DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
    #endregion
}
