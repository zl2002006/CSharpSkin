﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ComboBox.Struct
{
    #region 系统下拉框按钮状态
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 系统下拉框按钮状态
    /// </summary>
    public enum COMBOBOXBUTTONSTATE
    {
        STATE_SYSTEM_NONE = 0,
        STATE_SYSTEM_INVISIBLE = 0x00008000,
        STATE_SYSTEM_PRESSED = 0x00000008
    }
    #endregion
}
