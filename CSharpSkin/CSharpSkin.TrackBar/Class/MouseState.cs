﻿using CSharpSkin.TrackBar.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.TrackBar.Class
{
    #region 鼠标状态
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 鼠标状态
    /// </summary>
    public class MouseState
    {
        #region 是否按下左键
        /// <summary>
        /// 是否按下左键
        /// </summary>
        /// <returns></returns>
        public static bool IsMouseLeftKeyPress()
        {
            if (SystemInformation.MouseButtonsSwapped)
            {
                return (CSharpWinapi.GetKeyState(MOUSEBUTTONSTATE.RBUTTON) < 0);
            }
            else
            {
                return (CSharpWinapi.GetKeyState(MOUSEBUTTONSTATE.LBUTTON) < 0);
            }
        }
        #endregion
    }
    #endregion
}
