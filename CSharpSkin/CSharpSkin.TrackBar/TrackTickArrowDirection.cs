﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.TrackBar
{
    #region 滑动条滑块方向
    /// <summary>
    /// 滑动条滑块方向
    /// </summary>
    public enum TrackTickArrowDirection
    {
        None = 0,
        Left = 1,
        Right = 2,
        Up = 3,
        Down = 4,
        LeftRight = 5,
        UpDown = 6
    }
    #endregion
}
