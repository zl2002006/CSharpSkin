﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.TrackBar.Event
{
    public class PaintStateEventArgs: PaintEventArgs
    {
        private TrackBarState _trackBarState;
        public TrackBarState TrackBarState
        {
            get { return _trackBarState; }
        }

        public PaintStateEventArgs(Graphics g, Rectangle clipRect, TrackBarState state): base(g, clipRect)
        {
            _trackBarState = state;
        }
    }
}
