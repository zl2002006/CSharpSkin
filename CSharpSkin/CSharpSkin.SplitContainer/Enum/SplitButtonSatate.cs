﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.SplitContainer.Enum
{
    #region 分割按钮状态
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 分割按钮状态
    /// </summary>
    public enum SplitButtonSatate
    {
        None,
        Button,
        Spliter
    }
    #endregion
}
