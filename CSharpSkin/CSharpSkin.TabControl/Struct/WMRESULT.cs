﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.TabControl.Struct
{
    #region 系统消息结果
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 系统消息结果
    /// </summary>
    public static class WMRESULT
    {
        public static readonly IntPtr TRUE = new IntPtr(1);
        public static readonly IntPtr FALSE = new IntPtr(0);
    }
    #endregion
}
