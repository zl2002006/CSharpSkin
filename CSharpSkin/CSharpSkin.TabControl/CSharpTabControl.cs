﻿using CSharpSkin.TabControl.Class;
using CSharpSkin.TabControl.Event;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using CSharpSkin.TabControl.Struct;
using CSharpSkin.TabControl.Gdi;

namespace CSharpSkin.TabControl
{
    #region 带颜色的选项卡
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 带颜色的选项卡
    /// </summary>
    public class CSharpTabControl:System.Windows.Forms.TabControl
    {
        #region 定义成员
        /// <summary>
        /// 定义成员
        /// </summary>
        private TabControlUpDownButtonNativeWindow _tabControlUpDownButtonNativeWindow;
        private static readonly int _radius = 8;
        private static readonly object _eventPaintUpDownButton = new object();
        private const string UpDownButtonClassName = "msctls_updown32";
        internal IntPtr UpDownButtonHandle
        {
            get {
                return CSharpWinapi.FindWindowEx(base.Handle, IntPtr.Zero, UpDownButtonClassName, null);
            }
        }
        #endregion

        #region 基础颜色
        private Color _baseColor = Color.FromArgb(166, 222, 255);
        [Browsable(true)]
        [Description("基础颜色")]
        public Color BaseColor
        {
            get {
                return _baseColor;
            }
            set
            {
                _baseColor = value;
                this.Invalidate();
            }
        }
        #endregion

        #region 背景颜色
        private Color _backColor = Color.FromArgb(234, 247, 254);
        [Browsable(true)]
        [Description("背景颜色")]
        public override Color BackColor
        {
            get
            {
                return _backColor;
            }
            set
            {
                _backColor = value;
                this.Invalidate();
            }
        }
        #endregion

        #region 边框颜色
        /// <summary>
        /// 边框颜色
        /// </summary>
        private Color _borderColor = Color.FromArgb(23, 169, 254);
        [Browsable(true)]
        [Description("边框颜色")]
        public Color BorderColor
        {
            get
            {
                return _borderColor;
            }
            set
            {
                _borderColor = value;
                this.Invalidate();
            }
        }
        #endregion

        #region 按钮箭头颜色
        /// <summary>
        /// 按钮箭头颜色
        /// </summary>
        private Color _arrowColor = Color.FromArgb(0, 79, 125);
        [Browsable(true)]
        [Description("边框颜色")]
        public Color ArrowColor
        {
            get
            {
                return _arrowColor;
            }
            set
            {
                _arrowColor = value;
                this.Invalidate();
            }
        }
        #endregion

        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        public CSharpTabControl() : base()
        {
            //首先开启双缓冲，防止闪烁
            //双缓冲的设置 具体参数含义参照msdn的ControlStyles枚举值
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }
        #endregion

        #region 绑定添加事件
        /// <summary>
        /// 绑定添加事件
        /// </summary>
        public event TabControlUpDownButtonPaintEventHandler PaintTabControlUpDownButton
        {
            add { base.Events.AddHandler(_eventPaintUpDownButton, value); }
            remove { base.Events.RemoveHandler(_eventPaintUpDownButton, value); }
        }
        #endregion

        #region 绘制选项卡切换按钮
        /// <summary>
        /// 绘制选项卡切换按钮
        /// </summary>
        /// <param name="e"></param>
        public void OnPaintTabControlUpDownButton(TabControlUpDownButtonPaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Rectangle rect = e.ClipRectangle;
            Color upButtonBaseColor = _baseColor;
            Color upButtonBorderColor = _borderColor;
            Color upButtonArrowColor = _arrowColor;
            Color downButtonBaseColor = _baseColor;
            Color downButtonBorderColor = _borderColor;
            Color downButtonArrowColor = _arrowColor;
            Rectangle upButtonRect = rect;
            upButtonRect.X += 4;
            upButtonRect.Y += 4;
            upButtonRect.Width = rect.Width / 2 - 8;
            upButtonRect.Height -= 8;
            Rectangle downButtonRect = rect;
            downButtonRect.X = upButtonRect.Right + 2;
            downButtonRect.Y += 4;
            downButtonRect.Width = rect.Width / 2 - 8;
            downButtonRect.Height -= 8;
            if (Enabled)
            {
                if (e.IsMouseOver)
                {
                    if (e.IsMousePress)
                    {
                        if (e.IsMouseInUpButton)
                        {
                            upButtonBaseColor =ControlRender.GetColor(_baseColor, 0, -35, -24, -9);
                        }
                        else
                        {
                            downButtonBaseColor = ControlRender.GetColor(_baseColor, 0, -35, -24, -9);
                        }
                    }
                    else
                    {
                        if (e.IsMouseInUpButton)
                        {
                            upButtonBaseColor = ControlRender.GetColor(_baseColor, 0, 35, 24, 9);
                        }
                        else
                        {
                            downButtonBaseColor = ControlRender.GetColor(_baseColor, 0, 35, 24, 9);
                        }
                    }
                }
            }
            else
            {
                upButtonBaseColor = SystemColors.Control;
                upButtonBorderColor = SystemColors.ControlDark;
                upButtonArrowColor = SystemColors.ControlDark;
                downButtonBaseColor = SystemColors.Control;
                downButtonBorderColor = SystemColors.ControlDark;
                downButtonArrowColor = SystemColors.ControlDark;
            }
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Color backColor = Enabled ? _backColor : SystemColors.Control;
            using (SolidBrush brush = new SolidBrush(_backColor))
            {
                rect.Inflate(1, 1);
                g.FillRectangle(brush, rect);
            }

            RenderButton(g, upButtonRect,upButtonBaseColor,upButtonBorderColor,upButtonArrowColor,ArrowDirection.Left);
            RenderButton(g,downButtonRect,downButtonBaseColor,downButtonBorderColor,downButtonArrowColor, ArrowDirection.Right);
            TabControlUpDownButtonPaintEventHandler handler =base.Events[_eventPaintUpDownButton] as TabControlUpDownButtonPaintEventHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region 重写鼠标移动
        /// <summary>
        /// 重写鼠标移动
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            base.Invalidate();
        }
        #endregion

        #region 重写鼠标离开
        /// <summary>
        /// 重写鼠标离开
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            base.Invalidate();
        }
        #endregion

        #region 重绘控件
        /// <summary>
        /// 重绘控件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            DrawTabControl(e.Graphics);
        }
        #endregion

        #region 控件句柄创建时触发
        /// <summary>
        /// 控件句柄创建时触发
        /// </summary>
        /// <param name="e"></param>
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            if (UpDownButtonHandle != IntPtr.Zero)
            {
                if (_tabControlUpDownButtonNativeWindow == null)
                {
                    _tabControlUpDownButtonNativeWindow = new TabControlUpDownButtonNativeWindow(this);
                }
            }
        }
        #endregion

        #region 创建控件时触发
        /// <summary>
        /// 创建控件时触发
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            if (UpDownButtonHandle != IntPtr.Zero)
            {
                if (_tabControlUpDownButtonNativeWindow == null)
                {
                    _tabControlUpDownButtonNativeWindow = new TabControlUpDownButtonNativeWindow(this);
                }
            }
        }
        #endregion

        #region 句柄销毁时触发
        /// <summary>
        /// 句柄销毁时触发
        /// </summary>
        /// <param name="e"></param>
        protected override void OnHandleDestroyed(EventArgs e)
        {
            base.OnHandleDestroyed(e);
            if (_tabControlUpDownButtonNativeWindow != null)
            {
                _tabControlUpDownButtonNativeWindow.Dispose();
                _tabControlUpDownButtonNativeWindow = null;
            }
        }
        #endregion

        #region 添加控件时触发
        /// <summary>
        /// 添加控件时触发
        /// </summary>
        /// <param name="e"></param>
        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);

            if (UpDownButtonHandle != IntPtr.Zero)
            {
                if (_tabControlUpDownButtonNativeWindow == null)
                {
                    _tabControlUpDownButtonNativeWindow = new TabControlUpDownButtonNativeWindow(this);
                }
            }
        }
        #endregion

        #region 尺寸改变时触发
        /// <summary>
        /// 尺寸改变时触发
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (UpDownButtonHandle != IntPtr.Zero)
            {
                if (_tabControlUpDownButtonNativeWindow == null)
                {
                    _tabControlUpDownButtonNativeWindow = new TabControlUpDownButtonNativeWindow(this);
                }
            }
        }
        #endregion

        #region 绘制控件
        /// <summary>
        /// 绘制控件
        /// </summary>
        /// <param name="g"></param>
        private void DrawTabControl(Graphics g)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;
            g.TextRenderingHint = TextRenderingHint.AntiAlias;
            DrawBackgroundAndHeader(g);
            DrawTabPages(g);
            DrawBorder(g);
        }
        #endregion

        #region 绘制背景和头部
        /// <summary>
        /// 绘制背景和头部
        /// </summary>
        /// <param name="g"></param>
        private void DrawBackgroundAndHeader(Graphics g)
        {
            int x = 0;
            int y = 0;
            int width = 0;
            int height = 0;
            switch (Alignment)
            {
                case TabAlignment.Top:
                    x = 0;
                    y = 0;
                    width = ClientRectangle.Width;
                    height = ClientRectangle.Height - DisplayRectangle.Height;
                    break;
                case TabAlignment.Bottom:
                    x = 0;
                    y = DisplayRectangle.Height;
                    width = ClientRectangle.Width;
                    height = ClientRectangle.Height - DisplayRectangle.Height;
                    break;
                case TabAlignment.Left:
                    x = 0;
                    y = 0;
                    width = ClientRectangle.Width - DisplayRectangle.Width;
                    height = ClientRectangle.Height;
                    break;
                case TabAlignment.Right:
                    x = DisplayRectangle.Width;
                    y = 0;
                    width = ClientRectangle.Width - DisplayRectangle.Width;
                    height = ClientRectangle.Height;
                    break;
            }
            Rectangle headerRect = new Rectangle(x, y, width, height);
            Color backColor = Enabled ? _backColor : SystemColors.Control;
            using (SolidBrush brush = new SolidBrush(backColor))
            {
                g.FillRectangle(brush, ClientRectangle);
                g.FillRectangle(brush, headerRect);
            }
        }
        #endregion

        #region 绘制TabPage
        /// <summary>
        /// 绘制TabPage
        /// </summary>
        /// <param name="g"></param>
        private void DrawTabPages(Graphics g)
        {
            Rectangle tabRect;
            Point cusorPoint = PointToClient(MousePosition);
            bool hover;
            bool selected;
            bool hasSetClip = false;
            bool alignHorizontal =(Alignment == TabAlignment.Top ||Alignment == TabAlignment.Bottom);
            LinearGradientMode mode = alignHorizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
            if (alignHorizontal)
            {
                IntPtr upDownButtonHandle = UpDownButtonHandle;
                bool hasUpDown = upDownButtonHandle != IntPtr.Zero;
                if (hasUpDown)
                {
                    if (CSharpWinapi.IsWindowVisible(upDownButtonHandle))
                    {
                        RECT upDownButtonRect = new RECT();
                        CSharpWinapi.GetWindowRect(upDownButtonHandle, ref upDownButtonRect);
                        Rectangle upDownRect = Rectangle.FromLTRB(upDownButtonRect.Left,upDownButtonRect.Top,upDownButtonRect.Right,upDownButtonRect.Bottom);
                        upDownRect = RectangleToClient(upDownRect);
                        switch (Alignment)
                        {
                            case TabAlignment.Top:
                                upDownRect.Y = 0;
                                break;
                            case TabAlignment.Bottom:
                                upDownRect.Y =
                                    ClientRectangle.Height - DisplayRectangle.Height;
                                break;
                        }
                        upDownRect.Height = ClientRectangle.Height;
                        g.SetClip(upDownRect, CombineMode.Exclude);
                        hasSetClip = true;
                    }
                }
            }
            for (int index = 0; index < base.TabCount; index++)
            {
                TabPage page = TabPages[index];
                tabRect = GetTabRect(index);
                hover = tabRect.Contains(cusorPoint);
                selected = SelectedIndex == index;
                Color baseColor = _baseColor;
                Color borderColor = _borderColor;
                if (selected)
                {
                    baseColor = ControlRender.GetColor(_baseColor, 0, -45, -30, -14);
                }
                else if (hover)
                {
                    baseColor = ControlRender.GetColor(_baseColor, 0, 35, 24, 9);
                }
                RenderTabBackground(g,tabRect,baseColor,borderColor,0.45F,mode);
                bool hasImage = DrawTabImage(g, page, tabRect);
                DrawTabText(g, page, tabRect, hasImage);
            }
            if (hasSetClip)
            {
                g.ResetClip();
            }
        }
        #endregion

        #region 绘制Tab文本
        /// <summary>
        /// 绘制Tab文本
        /// </summary>
        /// <param name="g"></param>
        /// <param name="page"></param>
        /// <param name="tabRect"></param>
        /// <param name="hasImage"></param>
        private void DrawTabText(Graphics g, TabPage page, Rectangle tabRect, bool hasImage)
        {
            Rectangle textRect = tabRect;
            RectangleF newTextRect;
            StringFormat sf;
            switch (Alignment)
            {
                case TabAlignment.Top:
                case TabAlignment.Bottom:
                    if (hasImage)
                    {
                        textRect.X = tabRect.X + _radius / 2 + tabRect.Height - 2;
                        textRect.Width = tabRect.Width - _radius - tabRect.Height;
                    }

                    TextRenderer.DrawText(g,page.Text,page.Font,textRect,page.ForeColor);
                    break;
                case TabAlignment.Left:
                    if (hasImage)
                    {
                        textRect.Height = tabRect.Height - tabRect.Width + 2;
                    }
                    g.TranslateTransform(textRect.X, textRect.Bottom);
                    g.RotateTransform(270F);
                    sf = new StringFormat(StringFormatFlags.NoWrap);
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    sf.Trimming = StringTrimming.Character;
                    newTextRect = textRect;
                    newTextRect.X = 0;
                    newTextRect.Y = 0;
                    newTextRect.Width = textRect.Height;
                    newTextRect.Height = textRect.Width;
                    using (Brush brush = new SolidBrush(page.ForeColor))
                    {
                        g.DrawString(page.Text,page.Font,brush,newTextRect,sf);
                    }
                    g.ResetTransform();
                    break;
                case TabAlignment.Right:
                    if (hasImage)
                    {
                        textRect.Y = tabRect.Y + _radius / 2 + tabRect.Width - 2;
                        textRect.Height = tabRect.Height - _radius - tabRect.Width;
                    }
                    g.TranslateTransform(textRect.Right, textRect.Y);
                    g.RotateTransform(90F);
                    sf = new StringFormat(StringFormatFlags.NoWrap);
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    sf.Trimming = StringTrimming.Character;
                    newTextRect = textRect;
                    newTextRect.X = 0;
                    newTextRect.Y = 0;
                    newTextRect.Width = textRect.Height;
                    newTextRect.Height = textRect.Width;
                    using (Brush brush = new SolidBrush(page.ForeColor))
                    {
                        g.DrawString(page.Text,page.Font,brush,newTextRect,sf);
                    }
                    g.ResetTransform();
                    break;
            }
        }
        #endregion

        #region 绘制边框
        /// <summary>
        /// 绘制边框
        /// </summary>
        /// <param name="g"></param>
        private void DrawBorder(Graphics g)
        {
            if (SelectedIndex != -1)
            {
                Rectangle tabRect = GetTabRect(SelectedIndex);
                Rectangle clipRect = ClientRectangle;
                Point[] points = new Point[6];
                IntPtr upDownButtonHandle = UpDownButtonHandle;
                bool hasUpDown = upDownButtonHandle != IntPtr.Zero;
                if (hasUpDown)
                {
                    if (CSharpWinapi.IsWindowVisible(upDownButtonHandle))
                    {
                        RECT upDownButtonRect = new RECT();
                        CSharpWinapi.GetWindowRect(upDownButtonHandle,ref upDownButtonRect);
                        Rectangle upDownRect = Rectangle.FromLTRB(upDownButtonRect.Left,upDownButtonRect.Top,upDownButtonRect.Right,upDownButtonRect.Bottom);
                        upDownRect = RectangleToClient(upDownRect);
                        tabRect.X = tabRect.X > upDownRect.X ?upDownRect.X : tabRect.X;
                        tabRect.Width = tabRect.Right > upDownRect.X ?upDownRect.X - tabRect.X : tabRect.Width;
                    }
                }
                switch (Alignment)
                {
                    case TabAlignment.Top:
                        points[0] = new Point(tabRect.X,tabRect.Bottom);
                        points[1] = new Point(clipRect.X,tabRect.Bottom);
                        points[2] = new Point(clipRect.X,clipRect.Bottom - 1);
                        points[3] = new Point(clipRect.Right - 1,clipRect.Bottom - 1);
                        points[4] = new Point(clipRect.Right - 1,tabRect.Bottom);
                        points[5] = new Point(tabRect.Right,tabRect.Bottom);
                        break;
                    case TabAlignment.Bottom:
                        points[0] = new Point(tabRect.X,tabRect.Y);
                        points[1] = new Point(clipRect.X,tabRect.Y);
                        points[2] = new Point(clipRect.X,clipRect.Y);
                        points[3] = new Point(clipRect.Right - 1,clipRect.Y);
                        points[4] = new Point(clipRect.Right - 1,tabRect.Y);
                        points[5] = new Point(tabRect.Right,tabRect.Y);
                        break;
                    case TabAlignment.Left:
                        points[0] = new Point(tabRect.Right,tabRect.Y);
                        points[1] = new Point(tabRect.Right,clipRect.Y);
                        points[2] = new Point(clipRect.Right - 1,clipRect.Y);
                        points[3] = new Point(clipRect.Right - 1,clipRect.Bottom - 1);
                        points[4] = new Point(tabRect.Right,clipRect.Bottom - 1);
                        points[5] = new Point(tabRect.Right,tabRect.Bottom);
                        break;
                    case TabAlignment.Right:
                        points[0] = new Point(tabRect.X,tabRect.Y);
                        points[1] = new Point(tabRect.X,clipRect.Y);
                        points[2] = new Point(clipRect.X,clipRect.Y);
                        points[3] = new Point(clipRect.X, clipRect.Bottom - 1);
                        points[4] = new Point(tabRect.X,clipRect.Bottom - 1);
                        points[5] = new Point(tabRect.X,tabRect.Bottom);
                        break;
                }
                using (Pen pen = new Pen(_borderColor))
                {
                    g.DrawLines(pen, points);
                }
            }
        }
        #endregion

        #region 绘制箭头
        /// <summary>
        /// 绘制箭头
        /// </summary>
        /// <param name="g"></param>
        /// <param name="dropDownRect"></param>
        /// <param name="direction"></param>
        /// <param name="brush"></param>
        void RenderArrow(Graphics g,Rectangle dropDownRect,ArrowDirection direction,Brush brush)
        {
            Point point = new Point(dropDownRect.Left + (dropDownRect.Width / 2),dropDownRect.Top + (dropDownRect.Height / 2));
            Point[] points = null;
            switch (direction)
            {
                case ArrowDirection.Left:
                    points = new Point[] {new Point(point.X + 1, point.Y - 4),new Point(point.X + 1, point.Y + 4),new Point(point.X - 2, point.Y) };
                    break;
                case ArrowDirection.Up:
                    points = new Point[] {new Point(point.X - 3, point.Y + 1),new Point(point.X + 3, point.Y + 1),new Point(point.X, point.Y - 1) };
                    break;
                case ArrowDirection.Right:
                    points = new Point[] {new Point(point.X - 1, point.Y - 4),new Point(point.X - 1, point.Y + 4),new Point(point.X + 2, point.Y) };
                    break;
                default:
                    points = new Point[] {new Point(point.X - 3, point.Y - 1),new Point(point.X + 3, point.Y - 1),new Point(point.X, point.Y + 1) };
                    break;
            }
            g.FillPolygon(brush, points);
        }
        #endregion

        #region 绘制按钮
        /// <summary>
        /// 绘制按钮
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="baseColor"></param>
        /// <param name="borderColor"></param>
        /// <param name="arrowColor"></param>
        /// <param name="direction"></param>
        void RenderButton(Graphics g,Rectangle rect,Color baseColor,Color borderColor,Color arrowColor,ArrowDirection direction)
        {
            RenderBackground(g,rect,baseColor,borderColor,0.45f,true,LinearGradientMode.Vertical);
            using (SolidBrush brush = new SolidBrush(arrowColor))
            {
                RenderArrow(g,rect,direction,brush);
            }
        }
        #endregion

        #region 绘制背景
        /// <summary>
        /// 绘制背景
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="baseColor"></param>
        /// <param name="borderColor"></param>
        /// <param name="basePosition"></param>
        /// <param name="drawBorder"></param>
        /// <param name="mode"></param>
        void RenderBackground(Graphics g,Rectangle rect,Color baseColor,Color borderColor,float basePosition,bool drawBorder,LinearGradientMode mode)
        {
            using (LinearGradientBrush brush = new LinearGradientBrush(rect, Color.Transparent, Color.Transparent, mode))
            {
                Color[] colors = new Color[4];
                colors[0] = ControlRender.GetColor(baseColor, 0, 35, 24, 9);
                colors[1] = ControlRender.GetColor(baseColor, 0, 13, 8, 3);
                colors[2] = baseColor;
                colors[3] = ControlRender.GetColor(baseColor, 0, 68, 69, 54);
                ColorBlend blend = new ColorBlend();
                blend.Positions =new float[] { 0.0f, basePosition, basePosition + 0.05f, 1.0f };
                blend.Colors = colors;
                brush.InterpolationColors = blend;
                g.FillRectangle(brush, rect);
            }
            if (baseColor.A > 80)
            {
                Rectangle rectTop = rect;
                if (mode == LinearGradientMode.Vertical)
                {
                    rectTop.Height = (int)(rectTop.Height * basePosition);
                }
                else
                {
                    rectTop.Width = (int)(rect.Width * basePosition);
                }
                using (SolidBrush brushAlpha =new SolidBrush(Color.FromArgb(80, 255, 255, 255)))
                {
                    g.FillRectangle(brushAlpha, rectTop);
                }
            }
            if (drawBorder)
            {
                using (Pen pen = new Pen(borderColor))
                {
                    g.DrawRectangle(pen, rect);
                }
            }
        }
        #endregion

        #region 绘制选项背景
        /// <summary>
        /// 绘制选项背景
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="baseColor"></param>
        /// <param name="borderColor"></param>
        /// <param name="basePosition"></param>
        /// <param name="mode"></param>
        void RenderTabBackground(Graphics g, Rectangle rect,Color baseColor,Color borderColor,float basePosition, LinearGradientMode mode)
        {
            using (GraphicsPath path =GraphicsPathManager.CreateTabPath(rect,_radius,this.Alignment))
            {
                using (LinearGradientBrush brush = new LinearGradientBrush(rect, Color.Transparent, Color.Transparent, mode))
                {
                    Color[] colors = new Color[4];
                    colors[0] =ControlRender.GetColor(baseColor, 0, 35, 24, 9);
                    colors[1] =ControlRender.GetColor(baseColor, 0, 13, 8, 3);
                    colors[2] =baseColor;
                    colors[3] =ControlRender.GetColor(baseColor, 0, 68, 69, 54);

                    ColorBlend blend = new ColorBlend();
                    blend.Positions =
                        new float[] { 0.0f, basePosition, basePosition + 0.05f, 1.0f };
                    blend.Colors = colors;
                    brush.InterpolationColors = blend;
                    g.FillPath(brush, path);
                }
                if (baseColor.A > 80)
                {
                    Rectangle rectTop = rect;
                    if (mode == LinearGradientMode.Vertical)
                    {
                        rectTop.Height = (int)(rectTop.Height * basePosition);
                    }
                    else
                    {
                        rectTop.Width = (int)(rect.Width * basePosition);
                    }
                    using (SolidBrush brushAlpha =new SolidBrush(Color.FromArgb(80, 255, 255, 255)))
                    {
                        g.FillRectangle(brushAlpha, rectTop);
                    }
                }
                rect.Inflate(-1, -1);
                using (GraphicsPath path1 =GraphicsPathManager.CreateTabPath(rect, _radius,this.Alignment))
                {
                    using (Pen pen = new Pen(Color.FromArgb(255, 255, 255)))
                    {
                        if (Multiline)
                        {
                            g.DrawPath(pen, path1);
                        }
                        else
                        {
                            g.DrawLines(pen, path1.PathPoints);
                        }
                    }
                }
                using (Pen pen = new Pen(borderColor))
                {
                    if (Multiline)
                    {
                        g.DrawPath(pen, path);
                    }
                    {
                        g.DrawLines(pen, path.PathPoints);
                    }
                }
            }
        }
        #endregion

        #region 绘制图片
        /// <summary>
        /// 绘制图片
        /// </summary>
        /// <param name="g"></param>
        /// <param name="page"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        private bool DrawTabImage(Graphics g, TabPage page, Rectangle rect)
        {
            bool hasImage = false;
            if (ImageList != null)
            {
                Image image = null;
                if (page.ImageIndex != -1)
                {
                    image = ImageList.Images[page.ImageIndex];
                }
                else if (page.ImageKey != null)
                {
                    image = ImageList.Images[page.ImageKey];
                }
                if (image != null)
                {
                    hasImage = true;
                    Rectangle destRect = Rectangle.Empty;
                    Rectangle srcRect = new Rectangle(Point.Empty, image.Size);
                    switch (Alignment)
                    {
                        case TabAlignment.Top:
                        case TabAlignment.Bottom:
                            destRect = new Rectangle(rect.X + _radius / 2 + 2,rect.Y + 2,rect.Height - 4,rect.Height - 4);
                            break;
                        case TabAlignment.Left:
                            destRect = new Rectangle(
                                rect.X + 2,
                                rect.Bottom - (rect.Width - 4) - _radius / 2 - 2,
                                rect.Width - 4,rect.Width - 4);
                            break;
                        case TabAlignment.Right:
                            destRect = new Rectangle(rect.X + 2,rect.Y + _radius / 2 + 2,rect.Width - 4,rect.Width - 4);
                            break;
                    }
                    g.DrawImage(image,destRect, srcRect,GraphicsUnit.Pixel);
                }
            }
            return hasImage;
        }
        #endregion
    }
    #endregion
}
