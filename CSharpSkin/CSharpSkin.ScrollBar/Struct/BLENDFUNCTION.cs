﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar.Struct
{
    public struct BLENDFUNCTION
    {
        public byte BlendOp;
        public byte BlendFlags;
        public byte SourceConstantAlpha;
        public byte AlphaFormat;
        public BLENDFUNCTION(
            byte op, byte flags, byte alpha, byte format)
        {
            BlendOp = op;
            BlendFlags = flags;
            SourceConstantAlpha = alpha;
            AlphaFormat = format;
        }
    }
}
