﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar.Class
{
    public class ScrollBarNativeWindowBase: NativeWindowBase
    {
        private ScrollBarNativeWindow _scrollBarNativeWindow;

        public ScrollBarNativeWindowBase(ScrollBarNativeWindow scrollBarNativeWindow): base(scrollBarNativeWindow.ScrollBarHanlde)
        {
            _scrollBarNativeWindow = scrollBarNativeWindow;
        }

        protected override void OnPaint(IntPtr hWnd)
        {
            _scrollBarNativeWindow.DrawScrollBar(_scrollBarNativeWindow.ScrollBarHanlde, hWnd);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _scrollBarNativeWindow = null;
            }
            base.Dispose(disposing);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
