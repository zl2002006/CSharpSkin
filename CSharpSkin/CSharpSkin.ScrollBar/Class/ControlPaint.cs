﻿using CSharpSkin.ScrollBar.Gdi;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.ScrollBar.Class
{
    #region 绘制控件
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 绘制控件
    /// </summary>
    public class ControlPaint
    {
        #region 绘制选中状态
        /// <summary>
        /// 绘制选中状态
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="rect"></param>
        /// <param name="color"></param>
        public static void DrawChecked(Graphics graphics, Rectangle rect, Color color)
        {
            PointF[] points = new PointF[3];
            points[0] = new PointF(rect.X + rect.Width / 4.5f,rect.Y + rect.Height / 2.5f);
            points[1] = new PointF(rect.X + rect.Width / 2.5f,rect.Bottom - rect.Height / 3f);
            points[2] = new PointF(rect.Right - rect.Width / 4.0f,rect.Y + rect.Height / 4.5f);
            using (Pen pen = new Pen(color, 2F))
            {
                graphics.DrawLines(pen, points);
            }
        }
        #endregion

        #region 绘制区域
        /// <summary>
        /// 绘制区域
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="alphaCenter"></param>
        /// <param name="alphaSurround"></param>
        public static void DrawRect(Graphics g, RectangleF rect, int alphaCenter, int alphaSurround)
        {
            DrawRect(g, rect, Color.White, alphaCenter, alphaSurround);
        }
        #endregion

        #region 绘制区域
        /// <summary>
        /// 绘制区域
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="glassColor"></param>
        /// <param name="alphaCenter"></param>
        /// <param name="alphaSurround"></param>
        public static void DrawRect(Graphics g,RectangleF rect,Color glassColor,int alphaCenter,int alphaSurround)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddEllipse(rect);
                using (PathGradientBrush brush = new PathGradientBrush(path))
                {
                    brush.CenterColor = Color.FromArgb(alphaCenter, glassColor);
                    brush.SurroundColors = new Color[] {
                        Color.FromArgb(alphaSurround, glassColor) };
                    brush.CenterPoint = new PointF(
                        rect.X + rect.Width / 2,
                        rect.Y + rect.Height / 2);
                    g.FillPath(brush, path);
                }
            }
        }
        #endregion

        #region 绘制背景图片
        /// <summary>
        /// 绘制背景图片
        /// </summary>
        /// <param name="g"></param>
        /// <param name="backgroundImage"></param>
        /// <param name="backColor"></param>
        /// <param name="backgroundImageLayout"></param>
        /// <param name="bounds"></param>
        /// <param name="clipRect"></param>
        public static void DrawBackgroundImage(Graphics g,Image backgroundImage,Color backColor,ImageLayout backgroundImageLayout,Rectangle bounds,Rectangle clipRect)
        {
            DrawBackgroundImage(g,backgroundImage,backColor,backgroundImageLayout,bounds,clipRect,Point.Empty,RightToLeft.No);
        }
        #endregion

        #region 绘制背景图片
        /// <summary>
        /// 绘制背景图片
        /// </summary>
        /// <param name="g"></param>
        /// <param name="backgroundImage"></param>
        /// <param name="backColor"></param>
        /// <param name="backgroundImageLayout"></param>
        /// <param name="bounds"></param>
        /// <param name="clipRect"></param>
        /// <param name="scrollOffset"></param>
        public static void DrawBackgroundImage(Graphics g,Image backgroundImage,Color backColor,ImageLayout backgroundImageLayout,Rectangle bounds,Rectangle clipRect,Point scrollOffset)
        {
            DrawBackgroundImage(g,backgroundImage,backColor,backgroundImageLayout,bounds,clipRect,scrollOffset,RightToLeft.No);
        }
        #endregion

        #region 绘制背景图片
        /// <summary>
        /// 绘制背景图片
        /// </summary>
        /// <param name="g"></param>
        /// <param name="backgroundImage"></param>
        /// <param name="backColor"></param>
        /// <param name="backgroundImageLayout"></param>
        /// <param name="bounds"></param>
        /// <param name="clipRect"></param>
        /// <param name="scrollOffset"></param>
        /// <param name="rightToLeft"></param>
        public static void DrawBackgroundImage(Graphics g,Image backgroundImage,Color backColor,ImageLayout backgroundImageLayout,Rectangle bounds,Rectangle clipRect,Point scrollOffset,RightToLeft rightToLeft)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }
            if (backgroundImageLayout == ImageLayout.Tile)
            {
                using (TextureBrush brush = new TextureBrush(backgroundImage, WrapMode.Tile))
                {
                    if (scrollOffset != Point.Empty)
                    {
                        Matrix transform = brush.Transform;
                        transform.Translate((float)scrollOffset.X, (float)scrollOffset.Y);
                        brush.Transform = transform;
                    }
                    g.FillRectangle(brush, clipRect);
                    return;
                }
            }
            Rectangle rect = CalculateBackgroundImageRectangle(bounds,backgroundImage,backgroundImageLayout);
            if ((rightToLeft == RightToLeft.Yes) &&(backgroundImageLayout == ImageLayout.None))
            {
                rect.X += clipRect.Width - rect.Width;
            }
            using (SolidBrush brush2 = new SolidBrush(backColor))
            {
                g.FillRectangle(brush2, clipRect);
            }
            if (!clipRect.Contains(rect))
            {
                if ((backgroundImageLayout == ImageLayout.Stretch) ||(backgroundImageLayout == ImageLayout.Zoom))
                {
                    rect.Intersect(clipRect);
                    g.DrawImage(backgroundImage, rect);
                }
                else if (backgroundImageLayout == ImageLayout.None)
                {
                    rect.Offset(clipRect.Location);
                    Rectangle destRect = rect;
                    destRect.Intersect(clipRect);
                    Rectangle rectangle3 = new Rectangle(Point.Empty, destRect.Size);
                    g.DrawImage(backgroundImage,destRect,rectangle3.X,rectangle3.Y,rectangle3.Width,rectangle3.Height,GraphicsUnit.Pixel);
                }
                else
                {
                    Rectangle rectangle4 = rect;
                    rectangle4.Intersect(clipRect);
                    Rectangle rectangle5 = new Rectangle(new Point(rectangle4.X - rect.X, rectangle4.Y - rect.Y),rectangle4.Size);
                    g.DrawImage(backgroundImage,rectangle4,rectangle5.X,rectangle5.Y,rectangle5.Width,rectangle5.Height, GraphicsUnit.Pixel);
                }
            }
            else
            {
                ImageAttributes imageAttr = new ImageAttributes();
                imageAttr.SetWrapMode(WrapMode.TileFlipXY);
                g.DrawImage(backgroundImage, rect, 0, 0, backgroundImage.Width, backgroundImage.Height, GraphicsUnit.Pixel, imageAttr);
                imageAttr.Dispose();
            }
        }
        #endregion

        #region 计算背景区域
        /// <summary>
        /// 计算背景区域
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="backgroundImage"></param>
        /// <param name="imageLayout"></param>
        /// <returns></returns>
        public static Rectangle CalculateBackgroundImageRectangle(Rectangle bounds,Image backgroundImage,ImageLayout imageLayout)
        {
            Rectangle rectangle = bounds;
            if (backgroundImage != null)
            {
                switch (imageLayout)
                {
                    case ImageLayout.None:
                        rectangle.Size = backgroundImage.Size;
                        return rectangle;
                    case ImageLayout.Tile:
                        return rectangle;
                    case ImageLayout.Center:
                        {
                            rectangle.Size = backgroundImage.Size;
                            Size size = bounds.Size;
                            if (size.Width > rectangle.Width)
                            {
                                rectangle.X = (size.Width - rectangle.Width) / 2;
                            }
                            if (size.Height > rectangle.Height)
                            {
                                rectangle.Y = (size.Height - rectangle.Height) / 2;
                            }
                            return rectangle;
                        }
                    case ImageLayout.Stretch:
                        rectangle.Size = bounds.Size;
                        return rectangle;
                    case ImageLayout.Zoom:
                        {
                            Size size2 = backgroundImage.Size;
                            float num = ((float)bounds.Width) / ((float)size2.Width);
                            float num2 = ((float)bounds.Height) / ((float)size2.Height);
                            if (num >= num2)
                            {
                                rectangle.Height = bounds.Height;
                                rectangle.Width = (int)((size2.Width * num2) + 0.5);
                                if (bounds.X >= 0)
                                {
                                    rectangle.X = (bounds.Width - rectangle.Width) / 2;
                                }
                                return rectangle;
                            }
                            rectangle.Width = bounds.Width;
                            rectangle.Height = (int)((size2.Height * num) + 0.5);
                            if (bounds.Y >= 0)
                            {
                                rectangle.Y = (bounds.Height - rectangle.Height) / 2;
                            }
                            return rectangle;
                        }
                }
            }
            return rectangle;
        }
        #endregion

        #region 绘制滚动条滑块
        /// <summary>
        /// 绘制滚动条滑块
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <param name="orientation"></param>
        public static void DrawScrollBarTrack(Graphics g,Rectangle rect,Color begin,Color end,Orientation orientation)
        {
            bool bHorizontal = orientation == Orientation.Horizontal;
            LinearGradientMode mode = bHorizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
            Blend blend = new Blend();
            blend.Factors = new float[] { 1f, 0.5f, 0f };
            blend.Positions = new float[] { 0f, 0.5f, 1f };
            DrawGradientRect(g,rect,begin,end,begin,begin,blend,mode,true,false);
        }
        #endregion

        #region 绘制滚动条
        /// <summary>
        /// 绘制滚动条
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <param name="border"></param>
        /// <param name="innerBorder"></param>
        /// <param name="orientation"></param>
        /// <param name="changeColor"></param>
        public static void DrawScrollBar(Graphics g,Rectangle rect,Color begin,Color end, Color border,Color innerBorder,Orientation orientation,bool changeColor)
        {
            if (changeColor)
            {
                Color tmp = begin;
                begin = end;
                end = tmp;
            }
            bool bHorizontal = orientation == Orientation.Horizontal;
            LinearGradientMode mode = bHorizontal ? LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
            Blend blend = new Blend();
            blend.Factors = new float[] { 1f, 0.5f, 0f };
            blend.Positions = new float[] { 0f, 0.5f, 1f };
            if (bHorizontal)
            {
                rect.Inflate(0, -1);
            }
            else
            {
                rect.Inflate(-1, 0);
            }
            DrawGradientRoundRect(g, rect,begin,end,border,innerBorder,blend,mode,4,RoundStyle.All,true,true);
        }
        #endregion

        #region 绘制滚动条箭头
        /// <summary>
        /// 绘制滚动条箭头
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <param name="border"></param>
        /// <param name="innerBorder"></param>
        /// <param name="fore"></param>
        /// <param name="orientation"></param>
        /// <param name="arrowDirection"></param>
        /// <param name="changeColor"></param>
        public static void DrawScrollBarArraw(Graphics g,Rectangle rect,Color begin,Color end,Color border,Color innerBorder, Color fore, Orientation orientation,ArrowDirection arrowDirection,bool changeColor)
        {
            if (changeColor)
            {
                Color tmp = begin;
                begin = end;
                end = tmp;
            }
            bool bHorizontal = orientation == Orientation.Horizontal;
            LinearGradientMode mode = bHorizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
            rect.Inflate(-1, -1);
            Blend blend = new Blend();
            blend.Factors = new float[] { 1f, 0.5f, 0f };
            blend.Positions = new float[] { 0f, 0.5f, 1f };
            DrawGradientRoundRect(g,rect,begin,end,border,innerBorder,blend,mode,4,RoundStyle.All,true,true);
            using (SolidBrush brush = new SolidBrush(fore))
            {
                ControlRender.RenderArrow(
                    g,
                    rect,
                    arrowDirection,
                    brush);
            }
        }
        #endregion

        internal static void DrawGradientRect(Graphics g,Rectangle rect,Color begin,Color end,Color border,Color innerBorder,Blend blend,LinearGradientMode mode,bool drawBorder,bool drawInnerBorder)
        {
            using (LinearGradientBrush brush = new LinearGradientBrush(
                rect, begin, end, mode))
            {
                brush.Blend = blend;
                g.FillRectangle(brush, rect);
            }

            if (drawBorder)
            {
                System.Windows.Forms.ControlPaint.DrawBorder(g, rect, border, ButtonBorderStyle.Solid);
            }

            if (drawInnerBorder)
            {
                rect.Inflate(-1, -1);
                System.Windows.Forms.ControlPaint.DrawBorder(g, rect, border, ButtonBorderStyle.Solid);
            }
        }

        internal static void DrawGradientRoundRect(Graphics g,Rectangle rect,Color begin,Color end,Color border,Color innerBorder,Blend blend,LinearGradientMode mode,int radios,RoundStyle roundStyle,bool drawBorder,bool drawInnderBorder)
        {
            using (GraphicsPath path = GraphicsPathManager.CreatePath(rect, radios, roundStyle, true))
            {
                using (LinearGradientBrush brush = new LinearGradientBrush(rect, begin, end, mode))
                {
                    brush.Blend = blend;
                    g.FillPath(brush, path);
                }
                if (drawBorder)
                {
                    using (Pen pen = new Pen(border))
                    {
                        g.DrawPath(pen, path);
                    }
                }
            }

            if (drawInnderBorder)
            {
                rect.Inflate(-1, -1);
                using (GraphicsPath path = GraphicsPathManager.CreatePath(
                    rect, radios, roundStyle, true))
                {
                    using (Pen pen = new Pen(innerBorder))
                    {
                        g.DrawPath(pen, path);
                    }
                }
            }
        }
    }
    #endregion
}
