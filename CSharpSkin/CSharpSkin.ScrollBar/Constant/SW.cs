﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSkin.ScrollBar.Constant
{
    public class SW
    {
        private SW() { }

        public const int SW_HIDE = 0;
        public const int SW_NORMAL = 1;
        public const int SW_SHOWNOACTIVATE = 4;
    }
}
