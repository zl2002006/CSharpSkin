﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar.Constant
{
    #region 鼠标左右键状态值
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 鼠标左右键状态值
    /// </summary>
    public static class MOUSEBUTTONSTATE
    {
        public const int LBUTTON = 0x1;
        public const int RBUTTON = 0x2;
    }
    #endregion
}
