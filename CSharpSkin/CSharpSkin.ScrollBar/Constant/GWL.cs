﻿using System;

namespace CSharpSkin.ScrollBar.Constant
{
    public static class GWL
    {
        public const int GWL_WNDPROC = -4;
        public const int GWL_STYLE = -16;
        public const int GWL_EXSTYLE = -20;
    }
}
