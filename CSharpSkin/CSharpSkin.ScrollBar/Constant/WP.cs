﻿using System;

namespace CSharpSkin.ScrollBar.Constant
{ 
    public static class WP
    {
        public const uint WP_NOSIZE = 0x0001;
        public const uint WP_NOMOVE = 0x0002;
        public const uint WP_NOZORDER = 0x0004;
        public const uint WP_NOREDRAW = 0x0008;
        public const uint WP_NOACTIVATE = 0x0010;
        public const uint WP_FRAMECHANGED = 0x0020;
        public const uint WP_SHOWWINDOW = 0x0040;
        public const uint WP_HIDEWINDOW = 0x0080;
        public const uint WP_NOCOPYBITS = 0x0100;
        public const uint WP_NOOWNERZORDER = 0x0200;
        public const uint WP_NOSENDCHANGING = 0x0400;

        public const uint WP_DRAWFRAME = WP_FRAMECHANGED;
        public const uint WP_NOREPOSITION = WP_NOOWNERZORDER;

        #if(WINVER0400) //>= 0x0400
        public const uint WP_DEFERERASE = 0x2000;
        public const uint WP_ASYNCWINDOWPOS = 0x4000;
        #endif
    }
}
