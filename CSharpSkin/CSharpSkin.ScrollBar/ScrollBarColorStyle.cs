﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar
{
    #region 滚动条颜色样式
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 滚动条颜色样式
    /// </summary>
    public class ScrollBarColorStyle
    {
        #region 成员
        /// <summary>
        /// 成员
        /// </summary>
        private Color _baseColor = Color.FromArgb(171, 230, 247);
        private Color _backNormalColor = Color.FromArgb(235, 249, 253);
        private Color _backHoverColor = Color.FromArgb(121, 216, 243);
        private Color _backPressedColor = Color.FromArgb(70, 202, 239);
        private Color _borderColor = Color.FromArgb(89, 210, 249);
        private Color _innerBorderColor = Color.FromArgb(200, 250, 250, 250);
        private Color _foreColor = Color.FromArgb(48, 135, 192);
        #endregion

        #region 基础颜色
        /// <summary>
        /// 基础颜色
        /// </summary>
        public Color BaseColor { get => _baseColor; set => _baseColor = value; }
        #endregion

        #region 背景正常颜色
        /// <summary>
        /// 背景正常颜色
        /// </summary>
        public Color BackNormalColor { get => _backNormalColor; set => _backNormalColor = value; }
        #endregion

        #region 鼠标移入背景色
        /// <summary>
        /// 鼠标移入背景色
        /// </summary>
        public Color BackHoverColor { get => _backHoverColor; set => _backHoverColor = value; }
        #endregion

        #region 鼠标按下背景
        /// <summary>
        /// 鼠标按下背景
        /// </summary>
        public Color BackPressedColor { get => _backPressedColor; set => _backPressedColor = value; }
        #endregion

        #region 边框颜色
        /// <summary>
        /// 边框颜色
        /// </summary>
        public Color BorderColor { get => _borderColor; set => _borderColor = value; }
        #endregion

        #region 内部边框颜色
        /// <summary>
        /// 内部边框颜色
        /// </summary>
        public Color InnerBorderColor { get => _innerBorderColor; set => _innerBorderColor = value; }
        #endregion

        #region 字体颜色
        /// <summary>
        /// 字体颜色
        /// </summary>
        public Color ForeColor { get => _foreColor; set => _foreColor = value; }
        #endregion
    }
    #endregion
}
