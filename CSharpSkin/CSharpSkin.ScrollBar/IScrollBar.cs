﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar
{
    public interface IScrollBar
    {
        ScrollBarColorStyle ScrollBarColorStyle { get; set; }
    }
}
